import java.util.Scanner;

public class AufsteigendeReihenfolge {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte gebe 3 Zahlen ein:");
		int zahl1 = input.nextInt();
		int zahl2 = input.nextInt();
		int zahl3 = input.nextInt();
		
		if (zahl1 > zahl2) {
			int temp = zahl1;
			zahl1 = zahl2;
			zahl2 = temp;
		} else if(zahl2 > zahl3) {
			int temp = zahl2;
			zahl2 = zahl3;
			zahl3 = temp;
		} else if(zahl1 > zahl2) {
			int temp = zahl1;
			zahl1 = zahl2;
			zahl2 = temp;
		}
		
		System.out.printf("Nummern in aufsteigender Reihenfolge: %s %s %s", zahl1, zahl2, zahl3);

	}

}
