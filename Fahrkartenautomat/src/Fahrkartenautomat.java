﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
       
       
       while (true) {
    	   //Erfassung der Bestellung
    	   // -------------------------------
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   
    	   // Geldeinwurf
	       // -------------------------------
	       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       // Fahrscheinausgabe
	       // -------------------------------
	       fahrkartenAusgeben();
	       
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	       
	       //Abschiedsnachricht
	       // -------------------------------
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
	       
	       warte(5000);
       } 
    }
    
    /**
     * Erfassung der Tickets und des zu zahlenden Betrages.
     * @return - Betrag, der gezahlt werden soll.
     */
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner erfassung = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double preis = 0;
    	double PREIS_EINZEL = 2.90;
    	double PREIS_TAGES = 8.60;
    	double PREIS_GRUPPE = 23.50;
    	int anzahlDerTickets;
    	boolean bezahlen = false;
    	
    	String[] bezeichnungFahrkarten = { "Einzelfahrschein Berlin AB",
    										"Einzelfahrschein Berlin BC",
    										"Einzelfahrschein Berlin ABC",
    										"Kurzstrecke",
    										"Tageskarte Berlin AB",
    										"Tageskarte Berlin BC",
    										"Tageskarte Berlin ABC",
    										"Kleingruppen-Tageskarte Berlin AB",
    										"Kleingruppen-Tageskarte Berlin BC",
    										"Kleingruppen-Tageskarte Berlin ABC"
    										};
    	double[] fahrkartenPreise = { 2.90, 3.30, 3.60, 1.90, 8.60,
    									9.00, 9.60, 23.50, 24.30, 24.90 };
    	
    	while(bezahlen == false ) {
    		/*
    		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
    				+ "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    				+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    				+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
    				+ "Bezahlen (9)\n");
    		System.out.println("Ihre Wahl:");
    		*/
    		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:");
    		for (int i = 0;i < bezeichnungFahrkarten.length;i++) {
    			System.out.printf(" %d. %s [%.2f EUR]\n",i + 1 , bezeichnungFahrkarten[i], fahrkartenPreise[i]);
    		}
    		System.out.println("100. Exit");
    		
    		int ticketwahl = erfassung.nextInt();
    	
    		
    		if (ticketwahl < bezeichnungFahrkarten.length && ticketwahl != 100) {
    			preis = fahrkartenPreise[ticketwahl - 1];
    		} else if (ticketwahl == 100){
    			bezahlen = true;
    		} else {
    			System.out.println("Sie haben eine falsche Nummer eingegeben!");
    			warte(2000);
    			continue;
    		}
    
    		
    		
    		/*
    		
    		switch (ticketwahl) {
    		case 1:
    			preis = PREIS_EINZEL;
    			break;
    		case 2:
    			preis = PREIS_TAGES;
    			break;
    		case 3:
    			preis = PREIS_GRUPPE;
    			break;
    		case 9:
    			bezahlen = true;
    			break;
    		default:
    			System.out.println("Es wurde eine falsche Nummer eingegeben, bitte nutze nur die Nummer 1,2,3,9.");
    			continue;
    		}
    		
    		*/
    		
    		if (bezahlen == false) {
    			System.out.print("Anzahl der Tickets?\n");
    			anzahlDerTickets = erfassung.nextInt();
    			while ( anzahlDerTickets > 10 || anzahlDerTickets < 1) {
    				System.out.println("----------\nFalsche Ticketanzahl! Du kannst nur zwischen 1 - 10 Tickets kaufen. Bitte gebe eine richtige Zahl ein!");
    				anzahlDerTickets = erfassung.nextInt();
    			}
    			zuZahlenderBetrag = zuZahlenderBetrag + (preis * anzahlDerTickets); 
    		}
    	}

        return zuZahlenderBetrag;
    }
    
    /**
     * 
     * @param zuZahlenderBetrag	-	Betrag, der bezahlt werden soll.
     * @return - Gibt den eingezahlten Gesamtbetrag zurück
     */
   
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0;
    	double eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
    	

        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s : %.2f %n", "Noch zu zahlen", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");   	  
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
       return eingezahlterGesamtbetrag;
    }
    
    /**
     * Methode, welche die Fahrkarte ausgibt.
     */
    public static void fahrkartenAusgeben() {
    	
    	for (int i = 0; i < 8; i++) {
    		System.out.print("=");
    		warte(250);
    	}
    	System.out.println("\n\n");
    }
    
    /**
     * Berechnet das Rückgeld & gibt dieses aus.
     * @param eingezahlterGesamtbetrag	-	Der eingezahlte Gesamtbetrag
     * @param zuZahlenderBetrag	-	Betrag, der gezahlt werden soll. 
     */
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

     	   while (rückgabebetrag >= 2.0) {	// 2 Euro-Münze
     		   muenzeAusgeben(2, "Euro");
     		   rückgabebetrag -= 2.0;
     	   }
     	   while (rückgabebetrag >= 1.0) {	// 1 Euro-Münze
     		   muenzeAusgeben(1, "Euro");
     		   rückgabebetrag -= 1.0;
     	   }
     	   while (rückgabebetrag >= 0.5) {	// 50 Cent-Münze
     		   muenzeAusgeben(50, "Cent");
     		   rückgabebetrag -= 0.5;
     	   }
     	   while (rückgabebetrag >= 0.2) {	// 20 Cent-Münze
     		   muenzeAusgeben(20, "Cent");
     		   rückgabebetrag -= 0.2;
     	   }
     	   while (rückgabebetrag >= 0.1) {	// 10 Cent-Münze
     		   muenzeAusgeben(10, "Cent");
     		   rückgabebetrag -= 0.1;
     	   }
     	   while (rückgabebetrag >= 0.05) {	// 5 Cent-Münze
     		   muenzeAusgeben(5, "Cent");
     		   rückgabebetrag -= 0.05;
     	   }
        }
    }
    
    /**
     * Methode, um das Program zu verzögern.
     * @param millisekunde	-	Millisekunden, die gewartet werden sollen
     */
    static void warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     * 
     * @param betrag	-	integer, der aus Münze ausgegeben werden soll.
     * @param einheit	-	String, der angibt in welcher Variante der Betrag angegeben wird(Euro, Cent).
     */
    static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
}